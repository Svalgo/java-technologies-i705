package com.Sander.Homework1;

/**
 * Created by Sander on 6.04.2017.
 */
public class Alphabet {

    private final static Letter A = new Letter(
            "  A   ",
            " A A  ",
            "AAAAA ",
            "A   A ",
            "A   A ");
    private final static Letter B = new Letter(
            "BBB  ",
            "B  B ",
            "BBBB ",
            "B  B ",
            "BBB  ");
    private final static Letter C = new Letter(
            " CCC  ",
            "C   C ",
            "C     ",
            "C   C ",
            " CCC  ");
    private final static Letter D = new Letter(
            "DDD  ",
            "D  D ",
            "D  D ",
            "D  D ",
            "DDD  ");
    private final static Letter E = new Letter(
            "EEEE ",
            "E    ",
            "EEE  ",
            "E    ",
            "EEEE ");
    private final static Letter F = new Letter(
            "FFFF ",
            "F    ",
            "FFF  ",
            "F    ",
            "F    ");
    private final static Letter G = new Letter(
            " GGG ",
            "G    ",
            "G GG ",
            "G  G ",
            " GG  ");
    private final static Letter H = new Letter(
            "H   H ",
            "H   H ",
            "HHHHH ",
            "H   H ",
            "H   H ");
    private final static Letter I = new Letter(
            " II ",
            " II ",
            " II ",
            " II ",
            " II ");
    private final static Letter J = new Letter(
            "JJJJ ",
            "   J ",
            "   J ",
            " J J ",
            "  J  ");
    private final static Letter K = new Letter(
            "K  K ",
            "K K  ",
            "KK   ",
            "K K  ",
            "K  K ");
    private final static Letter L = new Letter(
            "L    ",
            "L    ",
            "L    ",
            "L    ",
            "LLLL ");
    private final static Letter M = new Letter(
            "M   M ",
            "MM MM ",
            "M M M ",
            "M   M ",
            "M   M ");
    private final static Letter N = new Letter(
            "N   N ",
            "NN  N ",
            "N N N ",
            "N  NN ",
            "N   N ");
    private final static Letter O = new Letter(
            " OO  ",
            "O  O ",
            "0  0 ",
            "O  O ",
            " OO  ");
    private final static Letter P = new Letter(
            "PPP  ",
            "P  P ",
            "PPP  ",
            "P    ",
            "P    ");
    private final static Letter Q = new Letter(
            " QQ  ",
            "Q  Q ",
            "Q  Q ",
            "Q QQ ",
            " QQQ ");
    private final static Letter R = new Letter(
            "RRR  ",
            "R  R ",
            "RRR  ",
            "R  R ",
            "R  R ");
    private final static Letter S = new Letter(
            " SSSS ",
            "SS    ",
            " SSS  ",
            "   SS ",
            "SSSS  ");
    private final static Letter T = new Letter(
            "TTTTTT ",
            "TTTTTT ",
            "  TT   ",
            "  TT   ",
            "  TT   ");
    private final static Letter U = new Letter(
            "U   U ",
            "U   U ",
            "U   U ",
            "U   U ",
            " UUU  ");
    private final static Letter V = new Letter(
            "V   V ",
            "V   V ",
            "V   V ",
            " VVV  ",
            "  V   ");
    private final static Letter W = new Letter(
            "W   W ",
            "W   W ",
            "W W W ",
            "WWWWW ",
            " W W  ");
    private final static Letter X = new Letter(
            "X   X ",
            " X X  ",
            "  X   ",
            " X X  ",
            "X   X ");
    private final static Letter Y = new Letter(
            "Y  Y ",
            "Y  Y ",
            " YY  ",
            "  Y  ",
            " YY  ");
    private final static Letter Z = new Letter(
            "ZZZZZ ",
            "   Z  ",
            "  Z   ",
            " Z    ",
            "ZZZZZ ");
    private final static Letter SPACE = new Letter("   ", "   ", "   ", "   ", "   ");
    private final static Letter NULL_LETTER = new Letter("", "", "", "", "");

    public Letter getLetter(char character) {
        switch (character) {
            case ('a'):return A;
            case ('b'):return B;
            case ('c'):return C;
            case ('d'):return D;
            case ('e'):return E;
            case ('f'):return F;
            case ('g'):return G;
            case ('h'):return H;
            case ('i'):return I;
            case ('j'):return J;
            case ('k'):return K;
            case ('l'):return L;
            case ('m'):return M;
            case ('n'):return N;
            case ('o'):return O;
            case ('p'):return P;
            case ('q'):return Q;
            case ('r'):return R;
            case ('s'):return S;
            case ('t'):return T;
            case ('u'):return U;
            case ('v'):return V;
            case ('w'):return W;
            case ('x'):return X;
            case ('y'):return Y;
            case ('z'):return Z;
            case (' '):return SPACE;
            default:return NULL_LETTER;
        }
    }
}

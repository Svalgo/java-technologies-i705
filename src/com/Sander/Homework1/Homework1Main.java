package com.Sander.Homework1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *  :::  :::  ::::   :   :::::: :::: :::     :::  :  :     :::::  :   :   : :::  :::: :::      ::  :   :
 * :   : :  : :     : :  :::::: :    :  :    :  : :  :    ::     : :  ::  : :  : :    :  :    :  : ::  :
 * :     :::  :::  :::::   ::   :::  :  :    ::::  ::      :::  ::::: : : : :  : :::  :::     :  : : : :
 * :   : :  : :    :   :   ::   :    :  :    :  :   :        :: :   : :  :: :  : :    :  :    :  : :  ::
 *  :::  :  : :::: :   :   ::   :::: :::     :::   ::     ::::  :   : :   : :::  :::: :  :     ::  :   :
 * 6.04.2017.
 * Non alphabet characters not supported for conversion
 */
public class Homework1Main {

    public static void main(String[] args) {
        char specialChar;
        String input = "";
        
        converter("Choose character");
        
        System.out.println("Type a character to use, to get started\nTo change the char, type: \"/\" \n" +
                "for example: /!?" +
                "\nTo Quit, type: Exit");
        BufferedReader askChar = new BufferedReader(new InputStreamReader(System.in));
        
        try {
            input = (askChar.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (input.length()>0){
            specialChar = input.charAt(0);
        }else{
            specialChar = ' ';
        }
        specialConverter("Character chosen", specialChar);
        feedMeText(specialChar);
    }

    private static void feedMeText(char specialChar) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        while (true) {
            try {
                input = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }if (input.length()>0 && input.charAt(0)=='/'){
                specialChar = input.charAt(1);
                specialConverter(input, specialChar);
            }
            else if (input.equalsIgnoreCase("exit")) {
                specialConverter("Exiting", specialChar);

                break;
            }
            else {
                specialConverter(input, specialChar);
            }
        }
    }

    private static void converter(String input) {
        specialConverter(input, ' ');
    }

    private static void specialConverter(String input, char specialChar) {
        Alphabet alphabet = new Alphabet();
        input = input.toLowerCase();
        for (int i = 0; i < 5; i++) {
            String printOut = "";
            for (char character : input.toCharArray()) {
                if (specialChar != ' ') {
                    for (char fromRow : alphabet.getLetter(character).getRow(i).toCharArray()) {
                        if (fromRow != ' ') {
                            String string = String.valueOf(specialChar);
                            printOut += string;
                        }
                        else {
                            printOut += " ";
                        }
                    }
                } else {
                    printOut += alphabet.getLetter(character).getRow(i);
                }
            }
            System.out.println(printOut);
        }
    }
}



package com.Sander.Homework1;

import java.util.ArrayList;


/**
 * Created by Sander on 6.04.2017.
 */
public class Letter {
    private ArrayList<String> theRows = new ArrayList<>();

    public Letter(String firstRow, String secondRow, String thirdRow, String fourthRow, String fifthRow) {
        this.theRows.add(firstRow);
        this.theRows.add(secondRow);
        this.theRows.add(thirdRow);
        this.theRows.add(fourthRow);
        this.theRows.add(fifthRow);

    }
    public String getRow(int row){
        return theRows.get(row);

    }
}


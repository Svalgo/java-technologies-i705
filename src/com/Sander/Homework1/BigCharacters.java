package com.Sander.Homework1;

/**
 * Created by svalgo on 11.04.2017.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;


public class BigCharacters {

    public static void main(String[] args) {

        int height = 50;
        int width = 100;

        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_BINARY);
        Graphics graphics = image.getGraphics();
        graphics.setColor(Color.white);
        graphics.fillRect(1, 1, 100, 50);
        graphics.setColor(Color.black);
        //graphics.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
        graphics.drawString("hello123456789".toUpperCase(), 0, 10);

        JFrame window = new JFrame() {
            @Override
            public void paint(Graphics g) {
                // TODO Auto-generated method stub
                super.paint(g);
                g.drawImage(image, 0, 0, null);
            }
        };
        window.setSize(width, height);
        //window.setVisible(true);
        image.getGraphics().setColor(Color.black);

        for (int y = 1; y <10 ; y++) {
            for (int x = 1; x < image.getWidth(); x++) {
                int rgb = image.getRGB(x, y);
                //image.getGraphics().fillRect(x, y, 10, 10);
                if(rgb==-1){
                    System.out.print(' ');
                }else{
                    System.out.print('#');
                }
            }
            System.out.println("");
        }
    }
}
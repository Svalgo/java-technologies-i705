# README #
Java Technologies I705


### What is this repository for? ###

* Java Technologies I705
* Homeworks

### How do I get set up? ###

* IDE or other prefered way for java.
* Configuration: choose Homework folder, then Homework#main.java
* Dependencies: only java 8

### Who do I talk to? ###

* Repo owner Sander Valgo
* Other community or team contact: None